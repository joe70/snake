/*
 * Window.h
 *
 *  Created on: 29 jun. 2019
 *      Author: joe
 */

#ifndef INCLUDE_WINDOW_H_
#define INCLUDE_WINDOW_H_

#include <string>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

class Window {
	private:
		void Setup(const std::string title, const sf::Vector2u& size);
		void Create();
		void Destroy();

		sf::RenderWindow m_window;
		sf::Vector2u m_windowSize;
		std::string m_windowTitle;
		bool m_isDone;
		bool m_isFullscreen;

	public:
		Window();
		Window(const std::string& title, const sf::Vector2u& size);
		~Window();

		void BeginDraw();
		void EndDraw();

		void Update();

		bool IsDone();
		bool IsFullscreen();
		sf::RenderWindow* GetRenderWindow();
		sf::Vector2u GetWindowSize();

		void ToggleFullscreen();

		void Draw(sf::Drawable& l_drawable);
};
#endif /* INCLUDE_WINDOW_H_ */
