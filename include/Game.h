/*
 * Game.h
 *
 *  Created on: 30 jun. 2019
 *      Author: joe
 */

#ifndef INCLUDE_GAME_H_
#define INCLUDE_GAME_H_

#include "Window.h"
#include "World.h"
#include "Snake.h"
#include "Textbox.h"

class Game {
private:

	Window m_window;

	sf::Clock m_clock;
	float m_elapsed;

	World m_world;
	Snake m_snake;
	Textbox m_textbox;

public:
	Game();
	~Game();

	void HandleInput();
	void Update();
	void Render();

	Window* GetWindow();

	sf::Time GetElapsed();
	void RestartClock();

};
#endif /* INCLUDE_GAME_H_ */
