EXEC:=Main
DIRSRC:=src/
DIROBJ:=obj/
DIRHEA:=include/

#Compilador
CXX:=g++
#Flags de compilación
CXXFLAGS:= -I/usr/include/SFML -Wall -I$(DIRHEA) -std=c++11
#Flagas de enlazado
LDFLAGS:= -lsfml-graphics -lsfml-window -lsfml-system -lsfml-network -lsfml-audio -lstdc++ -lm

ifeq ($(mode), release)
	CXXFLAGS+= -02 -D_RELEASE
else
	CXXFLAGS += -ggdb -D_DEBUG
	mode := debug
endif

#Obtención automática de la lista de objetos a compilar
OBJS:=$(subst $(DIRSRC), $(DIROBJ), $(patsubst %.cpp, %.o, $(wildcard $(DIRSRC)*.cpp)))

.PHONY: all clean

all: info $(EXEC)

info:
	@echo '---------------------------------------------------'
	@echo '>>> Using mode $(mode)'
	@echo '    (Please, call "make" with [mode=debug|release])'
	@echo '---------------------------------------------------'
	
# Enlazado -----------------------------------------
$(EXEC): $(OBJS)
	$(CXX) $^ -o $@ $(LDFLAGS)

# Compilación --------------------------------------
$(DIROBJ)%.o: $(DIRSRC)%.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

# Limpieza de temporales ---------------------------
clean:
	rm -f $(EXEC) *~ $(DIROBJ)* $(DIRSRC)*~ $(DIRHEA)*~